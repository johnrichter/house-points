package housepoints

import (
	"context"

	hpevent "gitlab.com/johnrichter/house-points/event"
)

type HousePointsStoragePlugin interface {
	PluginID() string
	AwardPoints(context.Context, *hpevent.HousePointsEvent, *hpevent.AwardPointsEvent) error
	LinkShared(context.Context, *hpevent.HousePointsEvent, *hpevent.LinkSharedEvent) error
	Reaction(context.Context, *hpevent.HousePointsEvent, *hpevent.ReactionEvent) error
}
