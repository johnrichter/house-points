# House Points

Interfaces for event generation, communication, and processing for the House Points game.

## Purpose

Key components of this library serve as shared definitions for

- House Points Events
- Generating and parsing House Points Events
- Communicating via an Event Bus
- Backend storage for House Points data.

Used by

- [House Points Manager](https://gitlab.com/johnrichter/house-points-manager)
- [House Points Worker](https://gitlab.com/johnrichter/house-points-worker)

# Development

Given the polyrepo setup of the House Points components, you may need to specify local locations for one or more
dependencies. Use the `go.mod.local` file as a guide to build and test this repo locally

In addition, see the [contribution guidelines](CONTRIBUTING.md).
