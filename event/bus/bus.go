package bus

import (
	"context"

	hpevent "gitlab.com/johnrichter/house-points/event"
)

type EventBusProducer interface {
	AddEvent(ctx context.Context, e *hpevent.HousePointsEvent) error
}

type EventBusConsumer interface {
	ProcessEvent(ctx context.Context, e *hpevent.HousePointsEvent) error
}
