package bus

import (
	"encoding/json"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/aws"
	sqstypes "github.com/aws/aws-sdk-go-v2/service/sqs/types"
	"github.com/rs/zerolog/log"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

const (
	SQSMessageAttributeAttemptKey = "retry-attempts-actual"
	DDTraceContextCarrierKey      = "datadog-trace-context"
)

type SQSMessageAttributes struct {
	RetryAttempt        int
	DdParentSpanContext ddtrace.SpanContext
}

func NewSQSMessageAttributes(retryAttempt int, ddParentSpanContext ddtrace.SpanContext) *SQSMessageAttributes {
	return &SQSMessageAttributes{RetryAttempt: retryAttempt, DdParentSpanContext: ddParentSpanContext}
}

func NewSQSMessageAttributesFromMessage(m *events.SQSMessage) *SQSMessageAttributes {
	ma := SQSMessageAttributes{RetryAttempt: -1, DdParentSpanContext: nil}
	ma.extractRetryAttempt(m)
	ma.extractDdParentSpanContext(m)
	return &ma
}

func (a *SQSMessageAttributes) extractRetryAttempt(m *events.SQSMessage) {
	var err error
	retryAttemptAttr, ok := m.MessageAttributes[SQSMessageAttributeAttemptKey]
	if !ok {
		log.Error().Msg("Missing attempt count attribute on SQS message")
	} else if a.RetryAttempt, err = strconv.Atoi(*retryAttemptAttr.StringValue); err != nil {
		log.Error().Err(err).Msg("Unable to read current SQS message processing attempt count")
	}
}

func (a *SQSMessageAttributes) extractDdParentSpanContext(m *events.SQSMessage) {
	var err error
	var ddTraceContextCarrier tracer.TextMapCarrier
	ddTraceContextCarrierAttr, ok := m.MessageAttributes[DDTraceContextCarrierKey]
	if !ok {
		log.Error().Msg("Missing datadog trace context attribute on SQS message")
	} else if err = json.Unmarshal(
		json.RawMessage(*ddTraceContextCarrierAttr.StringValue),
		&ddTraceContextCarrier,
	); err != nil {
		log.Error().Msg("Unable to unmarshal datadog trace context attribute on SQS message")
	} else if a.DdParentSpanContext, err = tracer.Extract(ddTraceContextCarrier); err != nil {
		log.Error().Msg("Unable to extract datadog span context from trace context carrier attribute on SQS message")
	}
}

func (a *SQSMessageAttributes) marshalRetryAttempt() string {
	return strconv.Itoa(a.RetryAttempt)
}

func (a *SQSMessageAttributes) marshalDdParentSpanContext() string {
	var rawTraceContextCarrier json.RawMessage
	traceContextCarrier := tracer.TextMapCarrier{}
	if err := tracer.Inject(a.DdParentSpanContext, traceContextCarrier); err != nil {
		log.Error().Msg("Unable to inject span context into text map carrier")
	} else if rawTraceContextCarrier, err = json.Marshal(traceContextCarrier); err != nil {
		log.Error().Msg("Unable to marshal trace context text map carrier")
	}
	return string(rawTraceContextCarrier)
}

func (a *SQSMessageAttributes) UpdateMessage(m *events.SQSMessage) {
	m.MessageAttributes[SQSMessageAttributeAttemptKey] = events.SQSMessageAttribute{
		DataType:    "Number",
		StringValue: aws.String(a.marshalRetryAttempt()),
	}
	m.MessageAttributes[DDTraceContextCarrierKey] = events.SQSMessageAttribute{
		DataType:    "String",
		StringValue: aws.String(a.marshalDdParentSpanContext()),
	}
}

func (a *SQSMessageAttributes) ToMessageAttributes(m *events.SQSMessage) map[string]sqstypes.MessageAttributeValue {
	attrs := map[string]sqstypes.MessageAttributeValue{}
	if m != nil {
		for k, ma := range m.MessageAttributes {
			attrs[k] = sqstypes.MessageAttributeValue{
				DataType:         &ma.DataType,
				StringValue:      ma.StringValue,
				StringListValues: ma.StringListValues,
				BinaryValue:      ma.BinaryValue,
				BinaryListValues: ma.BinaryListValues,
			}
		}
	}
	attrs[SQSMessageAttributeAttemptKey] = sqstypes.MessageAttributeValue{
		DataType:    aws.String("Number"),
		StringValue: aws.String(a.marshalRetryAttempt()),
	}
	attrs[DDTraceContextCarrierKey] = sqstypes.MessageAttributeValue{
		DataType:    aws.String("String"),
		StringValue: aws.String(a.marshalDdParentSpanContext()),
	}
	return attrs
}
