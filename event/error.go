package hpevent

import "errors"

var (
	ErrUnknownHousePointsEventType      = errors.New("unknown house points event type")
	ErrUnknownHousePointsSlackEventType = errors.New("unknown house points Slack event type")
	ErrStorageFailure                   = errors.New("unable to store house points")
	ErrMalformedSlackEventData          = errors.New("malformed Slack Events API data in house points event payload")
	ErrMalformedSlackEventCallbackData  = errors.New(
		"malformed Slack Events API callback data in house points event payload",
	)
	ErrMalformedSlackSlashCommandData = errors.New("malformed Slack Slash Command data in house points event payload")
	ErrMalformedSlackInteractionData  = errors.New(
		"malformed Slack Slash Interaction data in house points event payload",
	)
	ErrMarshalingSlackEventData = errors.New(
		"unable to marshal Slack Event data to house points event payload",
	)
	ErrInvalidHousePointsDialogSubmission = errors.New("invalid house points dialog submission")
)
