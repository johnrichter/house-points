package hpevent

import (
	"strconv"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

//
// EventsAPI Events
//

func GetLinkSharedEventFromSlackEvent(
	cbe *slackevents.EventsAPICallbackEvent,
	se *slackevents.LinkSharedEvent,
) *LinkSharedEvent {
	urls := []string{}
	for _, l := range se.Links {
		urls = append(urls, l.URL)
	}
	hpe := LinkSharedEvent{
		Type:         EventTypeLinkShared,
		Conversation: Conversation{ID: se.Channel},
		User:         User{ID: se.User},
		Urls:         urls,
	}
	return &hpe
}

func GetReactionEventFromSlackAddEvent(
	cbe *slackevents.EventsAPICallbackEvent,
	se *slackevents.ReactionAddedEvent,
) *ReactionEvent {
	re := ReactionEvent{
		Type:         EventTypeReaction,
		Action:       EventActionAdd,
		Benefactor:   User{ID: se.User},
		Beneficiary:  User{ID: se.ItemUser},
		Conversation: Conversation{ID: se.Item.Channel},
		Reaction:     se.Reaction,
	}
	return &re
}

func GetReactionEventFromSlackRemoveEvent(
	cbe *slackevents.EventsAPICallbackEvent,
	se *slackevents.ReactionRemovedEvent,
) *ReactionEvent {
	re := ReactionEvent{
		Type:         EventTypeReaction,
		Action:       EventActionRemove,
		Benefactor:   User{ID: se.User},
		Beneficiary:  User{ID: se.ItemUser},
		Conversation: Conversation{ID: se.Item.Channel},
		Reaction:     se.Reaction,
	}
	return &re
}

//
// Interactions
//

type HousePointsDialogSubmission struct {
	Action    string `json:"action"`
	Amount    int    `json:"amount"`
	Recipient string `json:"recipient"`
	Reason    string `json:"reason"`
}

func NewHousePointsDialogSubmission(submission map[string]string) (*HousePointsDialogSubmission, error) {
	action, actionOk := submission["action"]
	recipient, recipientOk := submission["recipient"]
	reason, reasonOk := submission["reason"]
	amount, amountOk := submission["amount"]
	amountI, amountIErr := strconv.Atoi(amount)
	if !actionOk || !recipientOk || !reasonOk || !amountOk || amountIErr != nil {
		return nil, ErrInvalidHousePointsDialogSubmission
	}
	s := HousePointsDialogSubmission{
		Action:    action,
		Amount:    amountI,
		Recipient: recipient,
		Reason:    reason,
	}
	return &s, nil
}

func GetAwardPointsEventFromSlackInteraction(
	cb *slack.InteractionCallback,
	ds *HousePointsDialogSubmission,
) *AwardPointsEvent {
	hpie := AwardPointsEvent{
		Type:         EventTypeAwardPoints,
		Conversation: Conversation{ID: cb.Channel.ID},
		Amount:       ds.Amount,
		Benefactor:   User{ID: ds.Recipient},
		Beneficiary:  User{ID: cb.User.ID},
		Explanation:  ds.Reason,
	}
	switch ds.Action {
	case EventActionAdd:
		hpie.Action = EventActionAdd
	case EventActionRemove:
		hpie.Action = EventActionRemove
	}
	return &hpie
}
