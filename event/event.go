package hpevent

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog/log"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

func WrapHousePointsInnerEvent(id, etype, origin string, e interface{}) *HousePointsEvent {
	eventData, err := json.Marshal(e)
	if err != nil {
		log.Error().Err(err).Msg("Unable to marshal House Points inner event")
	}
	return &HousePointsEvent{
		ID:        id,
		Type:      etype,
		Origin:    origin,
		Event:     e,
		EventData: eventData,
	}
}

func WrapHousePointsSlackInteraction(id, etype string, i *slack.InteractionCallback) (*HousePointsEvent, error) {
	slackInteractionData, err := json.Marshal(i)
	if err != nil {
		log.Error().Err(err).Msg("Unable to marshal House Points Slack Interaction event")
		return nil, ErrMarshalingSlackEventData
	}
	hpe := HousePointsEvent{
		ID:     id,
		Type:   etype,
		Origin: EventOriginSlack,
		Slack: HousePointsSlackEvent{
			Type:      SlackEventTypeInteraction,
			EventData: slackInteractionData,
		},
	}
	return &hpe, nil
}

func WrapHousePointsSlackEventsAPIEvent(
	etype string,
	cbe *slackevents.EventsAPICallbackEvent,
) (*HousePointsEvent, error) {
	slackEventData, err := json.Marshal(cbe)
	if err != nil {
		log.Error().Err(err).Msg("Unable to marshal House Points inner event")
		return nil, ErrMarshalingSlackEventData
	}
	hpe := HousePointsEvent{
		ID:     GenID("%s.%s.%s.%s.%s", etype, EventOriginSlack, cbe.APIAppID, cbe.TeamID, cbe.EventID),
		Type:   etype,
		Origin: EventOriginSlack,
		Slack: HousePointsSlackEvent{
			Type:      SlackEventTypeEventsAPI,
			EventData: slackEventData,
		},
	}
	return &hpe, nil
}

func GenID(fs string, i ...interface{}) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(fmt.Sprintf(fs, i...))))
}

func MarshalHousePointsEventToJson(e *HousePointsEvent) (string, error) {
	me, err := json.Marshal(e)
	if err != nil {
		return "", nil
	}
	return string(me), nil
}

func UnmarshalHousePointsEventFromJson(mes, slackVerificationToken string) (*HousePointsEvent, error) {
	var e HousePointsEvent
	meb := json.RawMessage(mes)
	if err := json.Unmarshal(meb, &e); err != nil {
		return nil, err
	}
	return unmarshalHousePointsEventData(&e, slackVerificationToken)
}

func UnmarshalHousePointsEventFromRawJson(
	meb json.RawMessage,
	slackVerificationToken string,
) (*HousePointsEvent, error) {
	var e HousePointsEvent
	if err := json.Unmarshal(meb, &e); err != nil {
		return nil, err
	}
	return unmarshalHousePointsEventData(&e, slackVerificationToken)
}

func unmarshalHousePointsEventData(hpe *HousePointsEvent, slackVerificationToken string) (*HousePointsEvent, error) {
	switch hpe.Type {
	case EventTypeAwardPoints:
		var e AwardPointsEvent
		if err := json.Unmarshal([]byte(hpe.EventData), &e); err != nil {
			return nil, err
		}
		hpe.Event = e
	case EventTypeLinkShared:
		var e LinkSharedEvent
		if err := json.Unmarshal([]byte(hpe.EventData), &e); err != nil {
			return nil, err
		}
		hpe.Event = e
	case EventTypeReaction:
		var e ReactionEvent
		if err := json.Unmarshal([]byte(hpe.EventData), &e); err != nil {
			return nil, err
		}
		hpe.Event = e
	default:
		return nil, ErrUnknownHousePointsEventType
	}
	if hpe.Origin == EventOriginSlack {
		if err := unmarshalHousePointsSlackEvent(hpe, slackVerificationToken); err != nil {
			return nil, err
		}
	}
	return hpe, nil
}

func unmarshalHousePointsSlackEvent(hpe *HousePointsEvent, slackVerificationToken string) error {
	switch hpe.Slack.Type {
	case SlackEventTypeSlashCommand:
		var cmd slack.SlashCommand
		err := json.Unmarshal(hpe.Slack.EventData, &cmd)
		if err != nil {
			log.Error().Err(err).Msg("Unable to parse Slack Slash Command data in house points event")
			return ErrMalformedSlackSlashCommandData
		}
		hpe.Slack.SlashCommand = cmd
	case SlackEventTypeInteraction:
		var interaction slack.InteractionCallback
		err := json.Unmarshal(hpe.Slack.EventData, &interaction)
		if err != nil {
			log.Error().Err(err).Msg("Unable to parse Slack Slash Command data in house points event")
			return ErrMalformedSlackInteractionData
		}
		hpe.Slack.Interaction = interaction
	case SlackEventTypeEventsAPI:
		ae, err := slackevents.ParseEvent(
			hpe.Slack.EventData,
			slackevents.OptionVerifyToken(
				&slackevents.TokenComparator{VerificationToken: slackVerificationToken},
			),
		)
		if err != nil {
			log.Error().Err(err).Msg("Unable to parse Slack Events API data in house points event")
			return ErrMalformedSlackEventData
		}
		hpe.Slack.EventsAPIEvent.Event = ae
		ecb, ok := ae.Data.(*slackevents.EventsAPICallbackEvent)
		if ok {
			hpe.Slack.EventsAPIEvent.Callback = *ecb
		} else {
			log.Error().Err(err).Msg("Parsed Slack Events API callback data is unsupported")
			return ErrMalformedSlackEventCallbackData
		}
	default:
		return ErrUnknownHousePointsSlackEventType
	}
	return nil
}
