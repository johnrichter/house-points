package housepoints

type HousePointsConfig struct {
	HousePointsLeaderboardURL      string `json:"house_points_leaderboard_url" mapstructure:"house_points_leaderboard_url"`
	HousePointsSubmissionURL       string `json:"house_points_submission_url" mapstructure:"house_points_submission_url"`
	HousePointsManualSubmissionURL string `json:"house_points_manual_submission_url" mapstructure:"house_points_manual_submission_url"`
}

func NewDefaultHousePointsConfig() *HousePointsConfig {
	return &HousePointsConfig{
		HousePointsLeaderboardURL:      "",
		HousePointsSubmissionURL:       "",
		HousePointsManualSubmissionURL: "",
	}
}
